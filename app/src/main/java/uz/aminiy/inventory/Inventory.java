package uz.aminiy.inventory;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by amin on 18.02.18.
 */

public class Inventory extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);

    }
}
