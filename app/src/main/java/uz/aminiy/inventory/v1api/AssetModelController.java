package uz.aminiy.inventory.v1api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import uz.aminiy.inventory.models.AssetModel;

public class AssetModelController {

    private static String BASE_URL = "http://192.168.1.42:8080/";
    private AssetModelController.AssetApi api;
    private Realm realm;

    public static void build() {

    }

    public AssetModelController() {
        this(BASE_URL);
    }

    public AssetModelController(String base_url) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")
                .setPrettyPrinting()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        api = retrofit.create(AssetApi.class);
        realm = Realm.getDefaultInstance();

    }

    public  void  deleteData() {
        RealmResults<AssetModel> assets = realm.where(AssetModel.class)
                .isNotNull("deleted_at").findAll();

        for (AssetModel assetModel : assets)
            api.delete(assetModel.getId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                assetModel.deleteFromRealm();
                            }
                        });
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("retro-delete-failed", assetModel.getId());
                }
            });
    }

    public  void  deleteRecords() {
        api.deletedIds().enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                if (response.isSuccessful()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<AssetModel> workspaces = realm.where(AssetModel.class)
                                    .in("id", response.body()).findAll();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {

            }
        });
    }


    public void getData() {

        api.listAll().enqueue(new Callback<List<AssetModel>>() {
            @Override
            public void onResponse(Call<List<AssetModel>> call, Response<List<AssetModel>> response) {

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(response.body());
                    }
                });
            }

            @Override
            public void onFailure(Call<List<AssetModel>> call, Throwable t) {
                Log.e("retro", t.toString());
            }
        });

    }


    public void sendData() {
        RealmResults<AssetModel> assets = realm.where(AssetModel.class)
                .isNotNull("edited_at").findAll();

        for (AssetModel assetModel : assets) {
            AssetModel assetmodelCopy = realm.copyFromRealm(assetModel);

            api.saveCall(assetmodelCopy).enqueue(new Callback<AssetModel>() {
                @Override
                public void onResponse(Call<AssetModel> call, Response<AssetModel> response) {
                    if (response.isSuccessful())
                    {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                assetModel.reset();
                                realm.copyToRealmOrUpdate(assetModel);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<AssetModel> call, Throwable t) {
                    Log.e("retro", t.toString());
                }
            });
        }
    }

    public interface AssetApi {
        @GET("api/v1/assetmodels/")
        Call<List<AssetModel>> listAll();

        @GET("api/v1/records/assetmodel/deleted/")
        Call<String[]> deletedIds();

        @POST("api/v1/assetmodels/")
        Call<AssetModel> saveCall(@Body AssetModel AssetModel);

        @DELETE("api/v1/assetmodels/{id}/")
        Call<ResponseBody> delete(@Path("id") String id);

    }
}
