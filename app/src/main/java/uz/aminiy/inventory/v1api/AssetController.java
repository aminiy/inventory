package uz.aminiy.inventory.v1api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import uz.aminiy.inventory.models.Asset;

public class AssetController {

    private static String BASE_URL = "http://192.168.1.42:8080/";
    private static String TOKEN = "";
    private AssetController.AssetApi api;
    private Realm realm;

    public static void build() {

    }

    public AssetController() {
        this(BASE_URL, TOKEN);
    }

    public AssetController(String base_url, String token) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")
                .setPrettyPrinting()
                .create();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
                                      @Override
                                      public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                                          Request original = chain.request();

                                          Request request = original.newBuilder()
                                                  .addHeader("Authorization", "Token " + token)
                                                  .method(original.method(), original.body())
                                                  .build();

                                          return chain.proceed(request);
                                      }
                                  });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        api = retrofit.create(AssetApi.class);
        realm = Realm.getDefaultInstance();

    }

    public  void  deleteData() {
        RealmResults<Asset> assets = realm.where(Asset.class)
                .isNotNull("deleted_at").findAll();

        for (Asset asset : assets)
            api.delete(asset.getId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                asset.deleteFromRealm();
                            }
                        });
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("retro-delete-failed", asset.getId());
                }
            });
    }

    public  void  deleteRecords() {
        api.deletedIds().enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                if (response.isSuccessful()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<Asset> workspaces = realm.where(Asset.class)
                                    .in("id", response.body()).findAll();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {

            }
        });
    }


    public void getData() {

        api.listAll().enqueue(new Callback<List<Asset>>() {
            @Override
            public void onResponse(Call<List<Asset>> call, Response<List<Asset>> response) {

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(response.body());
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Asset>> call, Throwable t) {
                Log.e("retro", t.toString());
            }
        });

    }


    public void sendData() {
        RealmResults<Asset> assets = realm.where(Asset.class)
                .isNotNull("edited_at").findAll();

        for (Asset asset : assets) {
            Asset assetCopy = realm.copyFromRealm(asset);

            api.saveCall(assetCopy).enqueue(new Callback<Asset>() {
                @Override
                public void onResponse(Call<Asset> call, Response<Asset> response) {
                    if (response.isSuccessful())
                    {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                asset.reset();
                                realm.copyToRealmOrUpdate(asset);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<Asset> call, Throwable t) {
                    Log.e("retro", t.toString());
                }
            });
        }
    }

    public interface AssetApi {
        @GET("api/v1/assets/")
        Call<List<Asset>> listAll();

        @GET("api/v1/records/asset/deleted/")
        Call<String[]> deletedIds();

        @POST("api/v1/assets/")
        Call<Asset> saveCall(@Body Asset asset);

        @DELETE("api/v1/assets/{id}/")
        Call<ResponseBody> delete(@Path("id") String id);

    }
}
