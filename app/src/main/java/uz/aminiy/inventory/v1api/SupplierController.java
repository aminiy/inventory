package uz.aminiy.inventory.v1api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import uz.aminiy.inventory.models.Supplier;

public class SupplierController {

    private static String BASE_URL = "http://192.168.1.42:8080/";
    private SupplierController.SupplierApi api;
    private Realm realm;

    public static void build() {

    }

    public SupplierController() {
        this(BASE_URL);
    }

    public SupplierController(String base_url) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")
                .setPrettyPrinting()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        api = retrofit.create(SupplierApi.class);
        realm = Realm.getDefaultInstance();

    }

    public  void  deleteData() {
        RealmResults<Supplier> suppliers = realm.where(Supplier.class)
                .isNotNull("deleted_at").findAll();

        for (Supplier supplier : suppliers)
            api.delete(supplier.getId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                supplier.deleteFromRealm();
                            }
                        });
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("retro-delete-failed", supplier.getId());
                }
            });
    }

    public  void  deleteRecords() {
        api.deletedIds().enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                if (response.isSuccessful()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<Supplier> Suppliers = realm.where(Supplier.class)
                                    .in("id", response.body()).findAll();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {

            }
        });
    }


    public void getData() {

        api.listAll().enqueue(new Callback<List<Supplier>>() {
            @Override
            public void onResponse(Call<List<Supplier>> call, Response<List<Supplier>> response) {

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(response.body());
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Supplier>> call, Throwable t) {
                Log.e("retro", t.toString());
            }
        });

    }


    public void sendData() {
        RealmResults<Supplier> suppliers = realm.where(Supplier.class)
                .isNotNull("edited_at").findAll();

        for (Supplier supplier : suppliers) {
            Supplier supplierCopy = realm.copyFromRealm(supplier);

            api.saveCall(supplierCopy).enqueue(new Callback<Supplier>() {
                @Override
                public void onResponse(Call<Supplier> call, Response<Supplier> response) {
                    if (response.isSuccessful())
                    {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                supplier.reset();
                                realm.copyToRealmOrUpdate(supplier);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<Supplier> call, Throwable t) {
                    Log.e("retro", t.toString());
                }
            });
        }
    }

    public interface SupplierApi {
        @GET("api/v1/suppliers/")
        Call<List<Supplier>> listAll();

        @GET("api/v1/records/supplier/deleted/")
        Call<String[]> deletedIds();

        @POST("api/v1/suppliers/")
        Call<Supplier> saveCall(@Body Supplier Supplier);

        @DELETE("api/v1/suppliers/{id}/")
        Call<ResponseBody> delete(@Path("id") String id);

    }
}
