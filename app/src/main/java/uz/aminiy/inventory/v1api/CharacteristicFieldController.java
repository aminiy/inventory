package uz.aminiy.inventory.v1api;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import uz.aminiy.inventory.models.AssetCharacteristic;

public class CharacteristicFieldController {

    private static String BASE_URL = "http://192.168.1.42:8080/";
    private CharacteristicFieldController.AssetCharacteristicApi api;
    private Realm realm;

    public static void build() {

    }

    public CharacteristicFieldController() {
        this(BASE_URL);
    }

    public CharacteristicFieldController(String base_url) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")
                .setPrettyPrinting()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        api = retrofit.create(AssetCharacteristicApi.class);
        realm = Realm.getDefaultInstance();

    }

    public void deleteData() {
        RealmResults<AssetCharacteristic> characteristics = realm.where(AssetCharacteristic.class)
                .isNotNull("deleted_at").findAll();

        for (AssetCharacteristic assetCharacteristic : characteristics)
            api.delete(assetCharacteristic.getId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                assetCharacteristic.deleteFromRealm();
                            }
                        });
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("retro-delete-failed", assetCharacteristic.getId());
                }
            });
    }

    public void deleteRecords() {
        api.deletedIds().enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                if (response.isSuccessful()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<AssetCharacteristic> CharacteristicField = realm.where(AssetCharacteristic.class)
                                    .in("id", response.body()).findAll();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {

            }
        });
    }


    public void getData() {

        api.listAll().enqueue(new Callback<List<AssetCharacteristic>>() {
            @Override
            public void onResponse(Call<List<AssetCharacteristic>> call, Response<List<AssetCharacteristic>> response) {

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(response.body());
                    }
                });
            }

            @Override
            public void onFailure(Call<List<AssetCharacteristic>> call, Throwable t) {
                Log.e("retro", t.toString());
            }
        });

    }


    public void sendData() {
        RealmResults<AssetCharacteristic> characteristics = realm.where(AssetCharacteristic.class)
                .isNotNull("edited_at").findAll();

        for (AssetCharacteristic assetCharacteristic : characteristics) {
            AssetCharacteristic assetcharacteristicCopy = realm.copyFromRealm(assetCharacteristic);

            api.saveCall(assetcharacteristicCopy).enqueue(new Callback<AssetCharacteristic>() {
                @Override
                public void onResponse(Call<AssetCharacteristic> call, Response<AssetCharacteristic> response) {
                    if (response.isSuccessful()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                assetCharacteristic.reset();
                                realm.copyToRealmOrUpdate(assetCharacteristic);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<AssetCharacteristic> call, Throwable t) {
                    Log.e("retro", t.toString());
                }
            });
        }
    }

    public interface AssetCharacteristicApi {
        @GET("api/v1/characteristicfields/")
        Call<List<AssetCharacteristic>> listAll();

        @GET("api/v1/records/characteristicfield/deleted/")
        Call<String[]> deletedIds();

        @POST("api/v1/characteristicfields/")
        Call<AssetCharacteristic> saveCall(@Body AssetCharacteristic AssetCharacteristic);

        @DELETE("api/v1/characteristicfields/{id}/")
        Call<ResponseBody> delete(@Path("id") String id);

    }
}
