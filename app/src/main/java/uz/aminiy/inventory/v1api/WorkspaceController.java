package uz.aminiy.inventory.v1api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import uz.aminiy.inventory.models.Workspace;

public class WorkspaceController {

    private static String BASE_URL = "http://192.168.1.42:8080/";
    private WorkspaceController.WorkspaceApi api;
    private Realm realm;

    public static void build() {

    }

    public WorkspaceController() {
        this(BASE_URL);
    }

    public WorkspaceController(String base_url) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")
                .setPrettyPrinting()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        api = retrofit.create(WorkspaceApi.class);
        realm = Realm.getDefaultInstance();

    }

    public  void  deleteData() {
        RealmResults<Workspace> workspaces = realm.where(Workspace.class)
                .isNotNull("deleted_at").findAll();

        for (Workspace workspace : workspaces)
            api.delete(workspace.getId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                workspace.deleteFromRealm();
                            }
                        });
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("retro-delete-failed", workspace.getId());
                }
            });
    }

    public  void  deleteRecords() {
        api.deletedIds().enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                if (response.isSuccessful()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<Workspace> workspaces = realm.where(Workspace.class)
                                    .in("id", response.body()).findAll();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {

            }
        });
    }


    public void getData() {

        api.listAll().enqueue(new Callback<List<Workspace>>() {
            @Override
            public void onResponse(Call<List<Workspace>> call, Response<List<Workspace>> response) {

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(response.body());
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Workspace>> call, Throwable t) {
                Log.e("retro", t.toString());
            }
        });

    }


    public void sendData() {
        RealmResults<Workspace> workspaces = realm.where(Workspace.class)
                .isNotNull("edited_at").findAll();

        for (Workspace workspace : workspaces) {
            Workspace workspaceCopy = realm.copyFromRealm(workspace);

            api.saveCall(workspaceCopy).enqueue(new Callback<Workspace>() {
                @Override
                public void onResponse(Call<Workspace> call, Response<Workspace> response) {
                    if (response.isSuccessful())
                    {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                workspace.reset();
                                realm.copyToRealmOrUpdate(workspace);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<Workspace> call, Throwable t) {
                    Log.e("retro", t.toString());
                }
            });
        }
    }

    public interface WorkspaceApi {
        @GET("api/v1/workspaces/")
        Call<List<Workspace>> listAll();

        @GET("api/v1/records/workspace/deleted/")
        Call<String[]> deletedIds();

        @POST("api/v1/workspaces/")
        Call<Workspace> saveCall(@Body Workspace Workspace);

        @DELETE("api/v1/workspaces/{id}/")
        Call<ResponseBody> delete(@Path("id") String id);

    }
}
