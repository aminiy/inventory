package uz.aminiy.inventory.v1api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import uz.aminiy.inventory.models.Department;

public class DepartmentController {

    private static String BASE_URL = "http://192.168.1.42:8080/";
    private DepartmentController.DepartmentApi api;
    private Realm realm;

    public static void build() {

    }

    public DepartmentController() {
        this(BASE_URL);
    }

    public DepartmentController(String base_url) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")
                .setPrettyPrinting()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        api = retrofit.create(DepartmentApi.class);
        realm = Realm.getDefaultInstance();

    }

    public  void  deleteData() {
        RealmResults<Department> departments = realm.where(Department.class)
                .isNotNull("deleted_at").findAll();

        for (Department department : departments)
            api.delete(department.getId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                department.deleteFromRealm();
                            }
                        });
                }
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("retro-delete-failed", department.getId());
                }
            });
    }

    public  void  deleteRecords() {
        api.deletedIds().enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                if (response.isSuccessful()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<Department> Departments = realm.where(Department.class)
                                    .in("id", response.body()).findAll();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {

            }
        });
    }


    public void getData() {

        api.listAll().enqueue(new Callback<List<Department>>() {
            @Override
            public void onResponse(Call<List<Department>> call, Response<List<Department>> response) {

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(response.body());
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Department>> call, Throwable t) {
                Log.e("retro", t.toString());
            }
        });

    }


    public void sendData() {
        RealmResults<Department> departments = realm.where(Department.class)
                .isNotNull("edited_at").findAll();

        for (Department department : departments) {
            Department departmentCopy = realm.copyFromRealm(department);

            api.saveCall(departmentCopy).enqueue(new Callback<Department>() {
                @Override
                public void onResponse(Call<Department> call, Response<Department> response) {
                    if (response.isSuccessful())
                    {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                department.reset();
                                realm.copyToRealmOrUpdate(department);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<Department> call, Throwable t) {
                    Log.e("retro", t.toString());
                }
            });
        }
    }

    public interface DepartmentApi {
        @GET("api/v1/departments/")
        Call<List<Department>> listAll();

        @GET("api/v1/records/department/deleted/")
        Call<String[]> deletedIds();

        @POST("api/v1/departments/")
        Call<Department> saveCall(@Body Department Department);

        @DELETE("api/v1/departments/{id}/")
        Call<ResponseBody> delete(@Path("id") String id);

    }
}
