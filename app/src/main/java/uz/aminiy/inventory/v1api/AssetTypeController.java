package uz.aminiy.inventory.v1api;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import uz.aminiy.inventory.models.AssetType;

public class AssetTypeController {

    private static String BASE_URL = "http://192.168.1.42:8080/";
    private AssetTypeController.AssetTypeApi api;
    private Realm realm;

    public static void build() {

    }

    public AssetTypeController() {
        this(BASE_URL);
    }

    public AssetTypeController(String base_url) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")
                .setPrettyPrinting()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        api = retrofit.create(AssetTypeApi.class);
        realm = Realm.getDefaultInstance();

    }

    public void deleteData() {
        RealmResults<AssetType> AssetType = realm.where(AssetType.class)
                .isNotNull("deleted_at").findAll();

        for (AssetType assettype : AssetType)
            api.delete(assettype.getId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                assettype.deleteFromRealm();
                            }
                        });
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("retro-delete-failed", assettype.getId());
                }
            });
    }

    public void deleteRecords() {
        api.deletedIds().enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {
                if (response.isSuccessful()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<AssetType> AssetType = realm.where(AssetType.class)
                                    .in("id", response.body()).findAll();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {

            }
        });
    }


    public void getData() {

        api.listAll().enqueue(new Callback<List<AssetType>>() {
            @Override
            public void onResponse(Call<List<AssetType>> call, Response<List<AssetType>> response) {

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.copyToRealmOrUpdate(response.body());
                    }
                });
            }

            @Override
            public void onFailure(Call<List<AssetType>> call, Throwable t) {
                Log.e("retro", t.toString());
            }
        });

    }


    public void sendData() {
        RealmResults<AssetType> assetTypes = realm.where(AssetType.class)
                .isNotNull("edited_at").findAll();

        for (AssetType assettype : assetTypes) {
            AssetType assettypeCopy = realm.copyFromRealm(assettype);

            api.saveCall(assettypeCopy).enqueue(new Callback<AssetType>() {
                @Override
                public void onResponse(Call<AssetType> call, Response<AssetType> response) {
                    if (response.isSuccessful()) {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                assettype.reset();
                                realm.copyToRealmOrUpdate(assettype);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<AssetType> call, Throwable t) {
                    Log.e("retro", t.toString());
                }
            });
        }
    }

    public interface AssetTypeApi {
        @GET("api/v1/assettypes/")
        Call<List<AssetType>> listAll();

        @GET("api/v1/records/assettype/deleted/")
        Call<String[]> deletedIds();

        @POST("api/v1/assettypes/")
        Call<AssetType> saveCall(@Body AssetType AssetType);

        @DELETE("api/v1/assettypes/{id}/")
        Call<ResponseBody> delete(@Path("id") String id);

    }
}
