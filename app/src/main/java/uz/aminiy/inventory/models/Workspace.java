package uz.aminiy.inventory.models;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by amin on 25.02.18.
 */

public class Workspace extends RealmObject {

    @PrimaryKey
    private String id;
    private Date edited_at;
    private Date deleted_at;
    private Department department;
    private String employee;
    private String post;

    public RealmResults<Asset> getAssets() {
        return assets;
    }

    @LinkingObjects("workspace")
    private final RealmResults<Asset> assets = null;

    @Override
    public String toString() {
        return employee;
    }

    public void setId(String id) {
        this.id = id;
        edited_at = new Date();
    }

    public void setEditedAt(Date edited_at) {
        this.edited_at = edited_at;
    }

    public Workspace() {
        id = UUID.randomUUID().toString();
    }

    public void setDepartment(Department department) {
        this.department = department;
        edited_at = new Date();
    }

    public void setEmployee(String employee) {
        this.employee = employee;
        edited_at = new Date();
    }

    public void setPost(String post) {
        this.post = post;
        edited_at = new Date();
    }

    public Date getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Date deleted_at) {
        this.deleted_at = deleted_at;
    }

    public void delete(){
        deleted_at = new Date();
    }

    public void reset() {
        edited_at = null;
    }

    public String getId() {
        return id;
    }

    public Date getEditedAt() {
        return edited_at;
    }

    public Department getDepartment() {
        return department;
    }

    public String getEmployee() {
        return employee;
    }

    public String getPost() {
        return post;
    }

}
