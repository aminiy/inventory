package uz.aminiy.inventory.models;

import android.databinding.BindingAdapter;
import android.databinding.InverseBindingAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by amin on 05.03.18.
 */

public class Asset extends RealmObject {
    @PrimaryKey
    private String id;
    private Date edited_at;
    private Date deleted_at;
    private String number;
    private String marker;
    private String serial_number;
    private String name;
    private AssetType type;
    private AssetModel model;
    private Date arrival_date;
    private Supplier supplier;
    private Workspace workspace;
    private Float summa;
    private Float remain;
    private Date inventory_date;
    @LinkingObjects("asset")
    private final RealmResults<AssetCharacteristic> characteristics = null;

    public RealmResults<AssetCharacteristic> getCharacteristics() {
        return characteristics;
    }

    public Asset() {
        id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        edited_at = new Date();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
        edited_at = new Date();
    }


    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
        edited_at = new Date();
    }

    public void delete() {
        deleted_at = new Date();
    }

    public void reset() {
        edited_at = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        edited_at = new Date();
    }

    public Date getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Date deleted_at) {
        this.deleted_at = deleted_at;
    }

    public AssetType getType() {
        return type;
    }

    public AssetModel getModel() {
        return model;
    }

    public void setModel(AssetModel model) {
        this.model = model;
        this.type = model.getAssettype();
        edited_at = new Date();
    }

    @BindingAdapter("android:text")
    public static void setText(TextView view, Date date) {
        if (date != null) {
            DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
            String localizedDate = df.format(date);

            view.setText(localizedDate);
        }
    }


    @BindingAdapter("android:text")
    public static void setText(TextView view, Float aFloat) {
        if (aFloat != null) {
            view.setText(aFloat.toString());
        }
    }

    @InverseBindingAdapter(attribute = "android:text", event = "android:textAttrChanged")
    public static Float captureTextValue(TextView view) {
        CharSequence newValue = view.getText();
        if (newValue.length()==0)
            newValue="0";
        return Float.parseFloat(newValue.toString());
    }

    public Date getArrival_date() {
        return arrival_date;
    }

    public void setArrival_date(Date arrival_date) {
        this.arrival_date = arrival_date;
        edited_at = new Date();
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
        edited_at = new Date();
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
        edited_at = new Date();
    }

    public Float getSumma() {
        return summa;
    }

    public void setSumma(Float summa) {
        this.summa = summa;
        edited_at = new Date();
    }

    public Float getRemain() {
        return remain;
    }

    public void setRemain(Float remain) {
        this.remain = remain;
        edited_at = new Date();
    }

    public Date getInventory_date() {
        return inventory_date;
    }

    public void setInventory_date(Date inventory_date) {
        this.inventory_date = inventory_date;
        edited_at = new Date();
    }

    public Date getEditedAt() {
        return edited_at;
    }

    public void setEditedAt(Date edited_at) {
        this.edited_at = edited_at;
    }
}
