package uz.aminiy.inventory.models;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by amin on 04.03.18.
 */

public class AssetModel extends RealmObject {
    @PrimaryKey
    private String id;
    private Date edited_at;
    private Date deleted_at;
    private String name;
    private AssetType assettype;
    @LinkingObjects("model")
    private final RealmResults<Asset> assets = null;

    public RealmResults<Asset> getAssets() {
        return assets;
    }

    public AssetModel() {
        id = UUID.randomUUID().toString();
    }

    @Override
    public String toString() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
        this.edited_at = new Date();
    }

    public Date getEditedAt() {
        return edited_at;
    }

    public void setEditedAt(Date edited_at) {
        this.edited_at = edited_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.edited_at = new Date();
    }

    public void delete() {
        deleted_at = new Date();
    }

    public void reset() {
        edited_at = null;
    }

    public Date getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Date deleted_at) {
        this.deleted_at = deleted_at;
    }

    public AssetType getAssettype() {
        return assettype;
    }

    public void setAssettype(AssetType assettype) {
        this.assettype = assettype;
        edited_at = new Date();
    }

}