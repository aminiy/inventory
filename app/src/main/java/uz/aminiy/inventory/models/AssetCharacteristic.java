package uz.aminiy.inventory.models;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by amin on 05.03.18.
 */

public class AssetCharacteristic extends RealmObject {
    @PrimaryKey
    private String id;
    private Date edited_at;
    private Date deleted_at;
    private Asset asset;
    private String asset_id;
    private CharacteristicField characteristic;
    private String value;

    public AssetCharacteristic() {
        id = UUID.randomUUID().toString();
    }

    public CharacteristicField getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(CharacteristicField characteristic) {
        this.characteristic = characteristic;
        edited_at = new Date();
    }

    @Override
    public String toString() {
        return value;
    }

    public String getId() {
        return id;
    }

    public String getAsset_id() {
        return asset_id;
    }

    public void setId(String id) {
        this.id = id;
        edited_at = new Date();
    }

    public Date getEditedAt() {
        return edited_at;
    }

    public void setEditedAt(Date edited_at) {
        this.edited_at = edited_at;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
        this.asset_id = asset.getId();
        edited_at = new Date();
    }

    public void delete() {
        deleted_at = new Date();
    }

    public void reset() {
        edited_at = null;
    }

    public Date getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Date deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        edited_at = new Date();
    }

}
