package uz.aminiy.inventory.models;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by amin on 17.02.18.
 */

public class Department extends RealmObject {
    @PrimaryKey
    private String id;
    private Date edited_at;
    private Date deleted_at;
    private String name;
    private String head;
    @LinkingObjects("department")
    private final RealmResults<Workspace> workspaces = null;

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
        edited_at = new Date();
    }

    public RealmResults<Workspace> getWorkspaces() {
        return workspaces;
    }

    public void setEditedAt(Date edited_at) {
        this.edited_at = edited_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        edited_at = new Date();
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
        edited_at = new Date();
    }

    public void delete() {
        deleted_at = new Date();
    }

    public void reset() {
        edited_at = null;
    }

    public Date getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Date deleted_at) {
        this.deleted_at = deleted_at;
    }

    public Department() {
        id = UUID.randomUUID().toString();
    }

    public Date getEditedAt() {
        return edited_at;
    }

}
