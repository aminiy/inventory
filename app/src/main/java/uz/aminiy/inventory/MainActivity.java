package uz.aminiy.inventory;

import android.os.Environment;
;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import io.realm.Realm;

import uz.aminiy.inventory.fragments.AssetEditorFragment;
import uz.aminiy.inventory.fragments.AssetModelEditorFragment;
import uz.aminiy.inventory.fragments.AssetTypeEditorFragment;
import uz.aminiy.inventory.fragments.CharacteristicFieldEditorFragment;
import uz.aminiy.inventory.fragments.DepartmentEditorFragment;
import uz.aminiy.inventory.fragments.ItemFragment;
import uz.aminiy.inventory.fragments.OnListFragmentInteractionListener;
import uz.aminiy.inventory.fragments.SearchFragment;
import uz.aminiy.inventory.fragments.SupplierEditorFragment;
import uz.aminiy.inventory.fragments.SyncFragment;
import uz.aminiy.inventory.fragments.WorkspaceEditorFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnListFragmentInteractionListener {

    private Realm realm;
    public int nav = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Realm.init(this);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        int id = item.getItemId();

        if (id != R.id.nav_search && id != R.id.nav_sync) {
            new ItemFragment();
            ItemFragment fragment = ItemFragment.newInstance(id);
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (id == R.id.nav_search) {
            SearchFragment fragment = new SearchFragment();
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }  else if (id==R.id.nav_sync) {
            SyncFragment fragment = new SyncFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }
        nav = id;
        return true;

    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public void onListFragmentInteraction(String item) {

        if (nav == R.id.nav_departments) {
            DepartmentEditorFragment fragment = DepartmentEditorFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (nav == R.id.nav_workspace) {
            WorkspaceEditorFragment fragment = WorkspaceEditorFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (nav == R.id.nav_asset_types) {
            AssetTypeEditorFragment fragment = AssetTypeEditorFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (nav == R.id.nav_asset_model) {
            AssetModelEditorFragment fragment = AssetModelEditorFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (nav == R.id.nav_all_characteristics) {
            CharacteristicFieldEditorFragment fragment = CharacteristicFieldEditorFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (nav == R.id.nav_suppliers) {
            SupplierEditorFragment fragment = SupplierEditorFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (nav == R.id.nav_assets || nav== R.id.nav_search) {
            AssetEditorFragment fragment = AssetEditorFragment.newInstance(item);
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (nav==R.id.nav_sync) {
            SyncFragment fragment = new SyncFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }

    }
}
