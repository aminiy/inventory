package uz.aminiy.inventory.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.SupplierItemBinding;
import uz.aminiy.inventory.fragments.OnListFragmentInteractionListener;
import uz.aminiy.inventory.models.Supplier;

/**
 * Created by amin on 04.03.18.
 */

public class SupplierAdapter extends RealmRecyclerViewAdapter<Supplier, SupplierAdapter.SupplierHolder> {
    private final OnListFragmentInteractionListener mListener;

    public SupplierAdapter(@Nullable OrderedRealmCollection<Supplier> data, boolean autoUpdate, OnListFragmentInteractionListener listener) {
        super(data, autoUpdate);

        mListener=listener;
    }

    @Override
    public SupplierAdapter.SupplierHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        SupplierItemBinding binding = SupplierItemBinding.inflate(inflater, parent, false);
        return new SupplierAdapter.SupplierHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(SupplierAdapter.SupplierHolder holder, int position) {
        Supplier item = getItem(position);
        holder.binding.setSupplier(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.binding.getSupplier().getId());
                }
            }
        });
    }

    class SupplierHolder extends RecyclerView.ViewHolder implements
            PopupMenu.OnMenuItemClickListener, View.OnClickListener {
        private SupplierItemBinding binding;

        private SupplierHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            Button button = (Button) itemView.findViewById(R.id.button);
            button.setOnClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            int id = item.getItemId();

            if (id == R.id.action_record_delete) {
                Realm realm = Realm.getDefaultInstance();
                Supplier Supplier = this.binding.getSupplier();
                if (Supplier!=null) {
                    if (Supplier.isValid()) {
                        realm.beginTransaction();
                        Supplier.delete();
                        realm.commitTransaction();
                    }
                }
                return false;
            }
            else if (id == R.id.action_record_edit)
                mListener.onListFragmentInteraction(binding.getSupplier().getId());

            return false;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(this.itemView.getContext(), v);
            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.crud);
            popup.show();

        }
    }
}
