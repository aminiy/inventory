package uz.aminiy.inventory.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.AssetModelItemBinding;
import uz.aminiy.inventory.databinding.DepartmentItemBinding;
import uz.aminiy.inventory.databinding.WorkspaceItemBinding;
import uz.aminiy.inventory.fragments.OnListFragmentInteractionListener;
import uz.aminiy.inventory.models.AssetModel;
import uz.aminiy.inventory.models.Department;
import uz.aminiy.inventory.models.Workspace;

/**
 * Created by amin on 04.03.18.
 */

public class AssetModelAdapter extends RealmRecyclerViewAdapter<AssetModel, AssetModelAdapter.AssetModelHolder> {
    private final OnListFragmentInteractionListener mListener;

    public AssetModelAdapter(@Nullable OrderedRealmCollection<AssetModel> data, boolean autoUpdate, OnListFragmentInteractionListener listener) {
        super(data, autoUpdate);

        mListener=listener;
    }

    @Override
    public AssetModelAdapter.AssetModelHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AssetModelItemBinding binding = AssetModelItemBinding.inflate(inflater, parent, false);
        return new AssetModelAdapter.AssetModelHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(AssetModelAdapter.AssetModelHolder holder, int position) {
        AssetModel item = getItem(position);
        holder.binding.setAssetmodel(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.binding.getAssetmodel().getId());
                }
            }
        });
    }

    class AssetModelHolder extends RecyclerView.ViewHolder implements
            PopupMenu.OnMenuItemClickListener, View.OnClickListener {
        private AssetModelItemBinding binding;

        private AssetModelHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            Button button = (Button) itemView.findViewById(R.id.button);
            button.setOnClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            int id = item.getItemId();

            if (id == R.id.action_record_delete) {
                Realm realm = Realm.getDefaultInstance();
                AssetModel assetmodel = this.binding.getAssetmodel();
                if (assetmodel!=null) {
                    if (assetmodel.isValid()) {
                        realm.beginTransaction();
                        assetmodel.delete();
                        realm.commitTransaction();
                    }
                }
                return false;
            }
            else if (id == R.id.action_record_edit)
                mListener.onListFragmentInteraction(binding.getAssetmodel().getId());

            return false;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(this.itemView.getContext(), v);
            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.crud);
            popup.show();

        }
    }
}
