package uz.aminiy.inventory.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.CharacteristicFieldItemBinding;
import uz.aminiy.inventory.fragments.OnListFragmentInteractionListener;
import uz.aminiy.inventory.models.CharacteristicField;

/**
 * Created by amin on 18.02.18.
 */

public class CharacteristicFieldAdapter extends RealmRecyclerViewAdapter<CharacteristicField, CharacteristicFieldAdapter.CharacteristicFieldHolder> {

    private final OnListFragmentInteractionListener mListener;

    public CharacteristicFieldAdapter(@Nullable OrderedRealmCollection<CharacteristicField> data, boolean autoUpdate, OnListFragmentInteractionListener listener) {
        super(data, autoUpdate);

        mListener=listener;
    }

    @Override
    public CharacteristicFieldHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        CharacteristicFieldItemBinding binding = CharacteristicFieldItemBinding.inflate(inflater, parent, false);
        return new CharacteristicFieldHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(CharacteristicFieldHolder holder, int position) {
        CharacteristicField item = getItem(position);
        holder.binding.setCharacteristic(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.binding.getCharacteristic().getId());
                }
            }
        });
    }



    class CharacteristicFieldHolder extends RecyclerView.ViewHolder implements
            PopupMenu.OnMenuItemClickListener, View.OnClickListener {
        private CharacteristicFieldItemBinding binding;

        private CharacteristicFieldHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            Button button = (Button) itemView.findViewById(R.id.button);
            button.setOnClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            int id = item.getItemId();

            if (id == R.id.action_record_delete) {
                Realm realm = Realm.getDefaultInstance();
                CharacteristicField department = this.binding.getCharacteristic();
                if (department!=null) {
                    if (department.isValid()) {
                        realm.beginTransaction();
                        department.delete();
                        realm.commitTransaction();
                    }
                }
                return false;
            }
            else if (id == R.id.action_record_edit)
                mListener.onListFragmentInteraction(binding.getCharacteristic().getId());

            return false;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(this.itemView.getContext(), v);
            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.crud);
            popup.show();

        }
    }
}
