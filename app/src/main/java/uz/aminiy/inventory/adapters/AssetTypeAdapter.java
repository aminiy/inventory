package uz.aminiy.inventory.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.AssetTypeItemBinding;
import uz.aminiy.inventory.databinding.DepartmentItemBinding;
import uz.aminiy.inventory.databinding.WorkspaceItemBinding;
import uz.aminiy.inventory.fragments.OnListFragmentInteractionListener;
import uz.aminiy.inventory.models.AssetType;
import uz.aminiy.inventory.models.Department;
import uz.aminiy.inventory.models.Workspace;

/**
 * Created by amin on 04.03.18.
 */

public class AssetTypeAdapter extends RealmRecyclerViewAdapter<AssetType, AssetTypeAdapter.AssetTypeHolder> {
    private final OnListFragmentInteractionListener mListener;

    public AssetTypeAdapter(@Nullable OrderedRealmCollection<AssetType> data, boolean autoUpdate, OnListFragmentInteractionListener listener) {
        super(data, autoUpdate);

        mListener=listener;
    }

    @Override
    public AssetTypeAdapter.AssetTypeHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AssetTypeItemBinding binding = AssetTypeItemBinding.inflate(inflater, parent, false);
        return new AssetTypeAdapter.AssetTypeHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(AssetTypeAdapter.AssetTypeHolder holder, int position) {
        AssetType dep = getItem(position);
        holder.binding.setAssetType(dep);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.binding.getAssetType().getId());
                }
            }
        });
    }

    class AssetTypeHolder extends RecyclerView.ViewHolder implements
            PopupMenu.OnMenuItemClickListener, View.OnClickListener {
        private AssetTypeItemBinding binding;

        private AssetTypeHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            Button button = (Button) itemView.findViewById(R.id.button);
            button.setOnClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            int id = item.getItemId();

            if (id == R.id.action_record_delete) {
                Realm realm = Realm.getDefaultInstance();
                AssetType assetType = this.binding.getAssetType();
                if (assetType!=null) {
                    if (assetType.isValid()) {
                        realm.beginTransaction();
                        assetType.delete();
                        realm.commitTransaction();
                    }
                }
                return false;
            }
            else if (id == R.id.action_record_edit)
                mListener.onListFragmentInteraction(binding.getAssetType().getId());

            return false;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(this.itemView.getContext(), v);
            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.crud);
            popup.show();

        }
    }
}
