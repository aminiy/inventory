package uz.aminiy.inventory.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import uz.aminiy.inventory.databinding.LinearCharacteristicBinding;
import uz.aminiy.inventory.models.AssetCharacteristic;


public class LinearAssetCharacteristics extends RealmRecyclerViewAdapter<AssetCharacteristic, LinearAssetCharacteristics.CharacteristicsHolder>{


    public LinearAssetCharacteristics(@Nullable OrderedRealmCollection<AssetCharacteristic> data, boolean autoUpdate) {
        super(data, autoUpdate);

    }

    @Override
    public CharacteristicsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LinearCharacteristicBinding binding = LinearCharacteristicBinding.inflate(inflater, parent, false);
        return new LinearAssetCharacteristics.CharacteristicsHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(CharacteristicsHolder holder, int position) {
        AssetCharacteristic item = getItem(position);
        holder.binding.setCharacteristic(item);
    }


    class CharacteristicsHolder extends RecyclerView.ViewHolder{

        private LinearCharacteristicBinding binding;

        public CharacteristicsHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
