package uz.aminiy.inventory.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.AssetItemBinding;
import uz.aminiy.inventory.fragments.OnListFragmentInteractionListener;
import uz.aminiy.inventory.models.Asset;

/**
 * Created by amin on 04.03.18.
 */

public class AssetAdapter extends RealmRecyclerViewAdapter<Asset, AssetAdapter.AssetHolder> {
    private final OnListFragmentInteractionListener mListener;

    public AssetAdapter(@Nullable OrderedRealmCollection<Asset> data, boolean autoUpdate, OnListFragmentInteractionListener listener) {
        super(data, autoUpdate);

        mListener=listener;
    }

    @Override
    public AssetAdapter.AssetHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AssetItemBinding binding = AssetItemBinding.inflate(inflater, parent, false);
        return new AssetAdapter.AssetHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(AssetAdapter.AssetHolder holder, int position) {
        Asset item = getItem(position);
        holder.binding.setAsset(item);
        holder.itemView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onListFragmentInteraction(holder.binding.getAsset().getId());
            }
        });
    }

    class AssetHolder extends RecyclerView.ViewHolder implements
            PopupMenu.OnMenuItemClickListener, View.OnClickListener {
        private AssetItemBinding binding;

        private AssetHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            Button button = itemView.findViewById(R.id.button);
            button.setOnClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            int id = item.getItemId();

            if (id == R.id.action_record_delete) {
                Realm realm = Realm.getDefaultInstance();
                Asset Asset = this.binding.getAsset();
                if (Asset!=null) {
                    if (Asset.isValid()) {
                        realm.beginTransaction();
                        Asset.delete();
                        realm.commitTransaction();
                    }
                }
                return false;
            }
            else if (id == R.id.action_record_edit)
                mListener.onListFragmentInteraction(binding.getAsset().getId());

            return false;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(this.itemView.getContext(), v);
            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.crud);
            popup.show();

        }
    }
}
