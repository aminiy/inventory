package uz.aminiy.inventory.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.DepartmentItemBinding;
import uz.aminiy.inventory.fragments.OnListFragmentInteractionListener;
import uz.aminiy.inventory.models.Department;

/**
 * Created by amin on 18.02.18.
 */

public class DepartmentAdapter extends RealmRecyclerViewAdapter<Department, DepartmentAdapter.DepartmentHolder> {

    private final OnListFragmentInteractionListener mListener;

    public DepartmentAdapter(@Nullable OrderedRealmCollection<Department> data, boolean autoUpdate, OnListFragmentInteractionListener listener) {
        super(data, autoUpdate);

        mListener=listener;
    }

    @Override
    public DepartmentHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        DepartmentItemBinding binding = DepartmentItemBinding.inflate(inflater, parent, false);
        return new DepartmentHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(DepartmentHolder holder, int position) {
        Department dep = getItem(position);
        holder.binding.setDepartment(dep);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.binding.getDepartment().getId());
                }
            }
        });
    }



    class DepartmentHolder extends RecyclerView.ViewHolder implements
            PopupMenu.OnMenuItemClickListener, View.OnClickListener {
        private DepartmentItemBinding binding;

        private DepartmentHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            Button button = (Button) itemView.findViewById(R.id.button);
            button.setOnClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            int id = item.getItemId();

            if (id == R.id.action_record_delete) {
                Realm realm = Realm.getDefaultInstance();
                Department department = this.binding.getDepartment();
                if (department!=null) {
                    if (department.isValid()) {
                        realm.beginTransaction();
                        department.delete();
                        realm.commitTransaction();
                    }
                }
                return false;
            }
            else if (id == R.id.action_record_edit)
                mListener.onListFragmentInteraction(binding.getDepartment().getId());

            return false;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(this.itemView.getContext(), v);
            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.crud);
            popup.show();

        }
    }
}
