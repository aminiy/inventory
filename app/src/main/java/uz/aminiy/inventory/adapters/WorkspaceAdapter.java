package uz.aminiy.inventory.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.WorkspaceItemBinding;
import uz.aminiy.inventory.fragments.OnListFragmentInteractionListener;
import uz.aminiy.inventory.models.Workspace;

/**
 * Created by amin on 25.02.18.
 */

public class WorkspaceAdapter extends RealmRecyclerViewAdapter<Workspace, WorkspaceAdapter.WorkspaceHolder> {

    private final OnListFragmentInteractionListener mListener;

    public WorkspaceAdapter(@Nullable OrderedRealmCollection<Workspace> data, boolean autoUpdate, OnListFragmentInteractionListener listener) {
        super(data, autoUpdate);
        mListener=listener;
    }

    @Override
    public WorkspaceAdapter.WorkspaceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        WorkspaceItemBinding binding = WorkspaceItemBinding.inflate(inflater, parent, false);
        return new WorkspaceAdapter.WorkspaceHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(WorkspaceAdapter.WorkspaceHolder holder, int position) {
        final Workspace item = getItem(position);
        holder.binding.setWorkspace(item);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.binding.getWorkspace().getId());
                }
            }
        });
    }



    class WorkspaceHolder extends RecyclerView.ViewHolder implements
            PopupMenu.OnMenuItemClickListener, View.OnClickListener {
        private WorkspaceItemBinding binding;

        private WorkspaceHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            Button button = (Button) itemView.findViewById(R.id.button);
            button.setOnClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {

            int id = item.getItemId();

            if (id == R.id.action_record_delete) {
                Realm realm = Realm.getDefaultInstance();
                Workspace workspace = this.binding.getWorkspace();
                if (workspace!=null) {
                    if (workspace.isValid()) {
                        realm.beginTransaction();
                        workspace.delete();
                        realm.commitTransaction();
                    }
                }
                return false;
            }
            else if (id == R.id.action_record_edit)
                mListener.onListFragmentInteraction(binding.getWorkspace().getId());

            return false;
        }

        @Override
        public void onClick(View v) {
            PopupMenu popup = new PopupMenu(this.itemView.getContext(), v);
            popup.setOnMenuItemClickListener(this);
            popup.inflate(R.menu.crud);
            popup.show();

        }
    }
}