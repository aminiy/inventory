package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import io.realm.Realm;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.FragmentCharacteristicFieldEditorBinding;
import uz.aminiy.inventory.models.AssetType;
import uz.aminiy.inventory.models.CharacteristicField;

public class CharacteristicFieldEditorFragment extends Fragment{

    private static final String AST_ID = "asset_type_id";

    private String getAstId;

    private Realm realm;
    private CharacteristicField model;

    public void setmListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
        this.isListener = true;
    }

    private Boolean isListener=false;
    private OnListFragmentInteractionListener mListener;
    FragmentCharacteristicFieldEditorBinding binding;

    public CharacteristicFieldEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param astid Parameter 1.
     * @return A new instance of fragment CharacteristicFieldEditorFragment.
     */

    public static CharacteristicFieldEditorFragment newInstance(String astid) {
        CharacteristicFieldEditorFragment fragment = new CharacteristicFieldEditorFragment();
        Bundle args = new Bundle();
        args.putString(AST_ID, astid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();

        if (getArguments() != null) {
            model = realm.where(CharacteristicField.class).equalTo("id", getArguments().getString(AST_ID)).findFirst();
        }
        else model = new CharacteristicField();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_characteristic_field_editor, container,false);
        binding.setCharacteristicfield(model);
        View view = binding.getRoot();


        if (!realm.isInTransaction())
            realm.beginTransaction();

        if (isListener) {
            Button button;
            button = view.findViewById(R.id.imageButton);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(v -> {
                if (null != mListener) {
                    realm.copyToRealmOrUpdate(model);
                    realm.commitTransaction();
                    mListener.onListFragmentInteraction(model.getId());
                }
            });
        }

        if (!realm.isInTransaction())
            realm.beginTransaction();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener==null)
                mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        if (realm.isInTransaction()) {
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();
        }

        super.onDestroyView();
    }
}
