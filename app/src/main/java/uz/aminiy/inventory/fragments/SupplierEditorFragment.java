package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import io.realm.Realm;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.FragmentSupplierEditorBinding;
import uz.aminiy.inventory.models.Supplier;

public class SupplierEditorFragment extends Fragment implements View.OnClickListener {

    private static final String AST_ID = "asset_type_id";

    private String getAstId;

    private Realm realm;
    private Supplier supplier;

    public void setmListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
        this.isListener = true;
    }

    private Boolean isListener=false;
    private OnListFragmentInteractionListener mListener;

    public SupplierEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param astid Parameter 1.
     * @return A new instance of fragment SupplierEditorFragment.
     */
    public static SupplierEditorFragment newInstance(String astid) {
        SupplierEditorFragment fragment = new SupplierEditorFragment();
        Bundle args = new Bundle();
        args.putString(AST_ID, astid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();

        if (getArguments() != null) {
            supplier = realm.where(Supplier.class).equalTo("id", getArguments().getString(AST_ID)).findFirst();
        }
        else supplier = new Supplier();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentSupplierEditorBinding binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_supplier_editor, container,false);
        binding.setSupplier(supplier);
        View view = binding.getRoot();

        Button button2 = view.findViewById(R.id.btnAsset);
        button2.setOnClickListener(this);

        if (!realm.isInTransaction())
            realm.beginTransaction();

        if (isListener) {
            Button button;
            button = view.findViewById(R.id.imageButton);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(v -> {
                if (null != mListener) {
                    realm.copyToRealmOrUpdate(supplier);
                    realm.commitTransaction();
                    mListener.onListFragmentInteraction(supplier.getId());
                }
            });
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener==null)
                mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        if (realm.isInTransaction()) {
            realm.copyToRealmOrUpdate(supplier);
            realm.commitTransaction();
        }

        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnAsset) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_assets, R.id.nav_suppliers, supplier.getId());
            fragment.setmListener(fragment);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }
    }
}
