package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import io.realm.Realm;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.FragmentAssetTypeEditorBinding;
import uz.aminiy.inventory.models.AssetType;

public class AssetTypeEditorFragment extends Fragment implements View.OnClickListener{

    private static final String AST_ID = "asset_type_id";

    private String getAstId;

    private Realm realm;
    private AssetType assetType;

    public void setmListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
        this.isListener = true;
    }

    private Boolean isListener=false;
    private OnListFragmentInteractionListener mListener;

    public AssetTypeEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param astid Parameter 1.
     * @return A new instance of fragment AssetTypeEditorFragment.
     */

    public static AssetTypeEditorFragment newInstance(String astid) {
        AssetTypeEditorFragment fragment = new AssetTypeEditorFragment();
        Bundle args = new Bundle();
        args.putString(AST_ID, astid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();

        if (getArguments() != null) {
            assetType = realm.where(AssetType.class).equalTo("id", getArguments().getString(AST_ID)).findFirst();
        }
        else assetType = new AssetType();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentAssetTypeEditorBinding binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_asset_type_editor, container,false);
        binding.setAssetType(assetType);
        View view = binding.getRoot();

        Button button2 = view.findViewById(R.id.btnAsset);
        button2.setOnClickListener(this);

        if (!realm.isInTransaction())
            realm.beginTransaction();

        if (isListener) {
            Button button;
            button = view.findViewById(R.id.imageButton);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(v -> {
                if (null != mListener) {
                    realm.copyToRealmOrUpdate(assetType);
                    realm.commitTransaction();
                    mListener.onListFragmentInteraction(assetType.getId());
                }
            });
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener==null)
                mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        if (realm.isInTransaction()) {
            realm.copyToRealmOrUpdate(assetType);
            realm.commitTransaction();
        }

        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {

        if (v.getId()==R.id.btnAsset) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_assets, R.id.nav_asset_types, assetType.getId());
            fragment.setmListener(fragment);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }
    }
}
