package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import io.realm.Realm;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.FragmentDepartmentEditorBinding;
import uz.aminiy.inventory.models.Department;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link DepartmentEditorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DepartmentEditorFragment extends Fragment implements View.OnClickListener {

    private static final String DEP_ID = "param1";

    private Realm realm;
    private Department department;

    public void setmListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
        this.isListener = true;
    }

    private Boolean isListener=false;
    private OnListFragmentInteractionListener mListener;

    public DepartmentEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param dep Parameter 1..
     * @return A new instance of fragment DepartmentEditorFragment.
     */
    public static DepartmentEditorFragment newInstance(String dep) {
        DepartmentEditorFragment fragment = new DepartmentEditorFragment();
        Bundle args = new Bundle();
        args.putString(DEP_ID, dep);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();

        FragmentDepartmentEditorBinding.inflate(getActivity().getLayoutInflater());
        Department dep;

        if (getArguments() != null) {
            dep = realm.where(Department.class).equalTo("id", getArguments().getString(DEP_ID)).findFirst();
        }
        else
            dep = new Department();

        department = dep;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentDepartmentEditorBinding binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_department_editor, container, false);
        binding.setDepartment(department);
        View view = binding.getRoot();

        Button button2 = view.findViewById(R.id.btnAsset);
        button2.setOnClickListener(this);

        if (!realm.isInTransaction())
            realm.beginTransaction();

        if (isListener) {
            Button button;
            button = view.findViewById(R.id.imageButton);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(v -> {
                if (null != mListener) {
                    realm.copyToRealmOrUpdate(department);
                    realm.commitTransaction();
                    mListener.onListFragmentInteraction(department.getId());
                }
            });
        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener==null)
                mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onDestroyView() {
        if (realm.isInTransaction()) {
            realm.copyToRealmOrUpdate(department);
            realm.commitTransaction();
        }
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnAsset) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_assets, R.id.nav_departments, department.getId());
            fragment.setmListener(fragment);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }
    }
}
