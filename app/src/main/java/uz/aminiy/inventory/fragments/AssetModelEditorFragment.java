package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import io.realm.Realm;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.FragmentAssetModelEditorBinding;
import uz.aminiy.inventory.models.AssetModel;
import uz.aminiy.inventory.models.AssetType;

public class AssetModelEditorFragment extends Fragment
        implements OnListFragmentInteractionListener,
        View.OnClickListener {

    private static final String AST_ID = "asset_type_id";

    private String getAstId;

    private Realm realm;
    private AssetModel model;

    public void setmListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
        this.isListener = true;
    }

    private Boolean isListener = false;
    private OnListFragmentInteractionListener mListener;
    FragmentAssetModelEditorBinding binding;

    public AssetModelEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param astid Parameter 1.
     * @return A new instance of fragment AssetModelEditorFragment.
     */
    public static AssetModelEditorFragment newInstance(String astid) {
        AssetModelEditorFragment fragment = new AssetModelEditorFragment();
        Bundle args = new Bundle();
        args.putString(AST_ID, astid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();

        if (getArguments() != null) {
            model = realm.where(AssetModel.class).equalTo("id", getArguments().getString(AST_ID)).findFirst();
        } else model = new AssetModel();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_asset_model_editor, container, false);
        binding.setModel(model);

        View view = binding.getRoot();

        if (!realm.isInTransaction())
            realm.beginTransaction();

        if (isListener) {
            Button button;
            button = view.findViewById(R.id.imageButton);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(v -> {
                if (null != mListener) {
                    realm.copyToRealmOrUpdate(model);
                    realm.commitTransaction();
                    mListener.onListFragmentInteraction(model.getId());
                }
            });
        }

        ImageButton button = view.findViewById(R.id.selectDep);
        button.setOnClickListener(this);
        ImageButton button1 = view.findViewById(R.id.addDep);
        button1.setOnClickListener(this);
        Button button2 = view.findViewById(R.id.btnAsset);
        button2.setOnClickListener(this);
        if (!realm.isInTransaction())
            realm.beginTransaction();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener == null)
                mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        if (realm.isInTransaction()) {
            realm.copyToRealmOrUpdate(model);
            realm.commitTransaction();
        }

        super.onDestroyView();
    }

    @Override
    public void onListFragmentInteraction(String item) {
        if (item != null) {
            realm.beginTransaction();
            AssetType type = realm.where(AssetType.class).equalTo("id", item).findFirst();
            if (type != null) {
                model.setAssettype(type);
                realm.copyToRealmOrUpdate(model);
                binding.setModel(model);
            }
            realm.commitTransaction();
        }
        if (!realm.isInTransaction())
            realm.beginTransaction();

        getFragmentManager().popBackStack();

    }

    @Override
    public void onClick(View v) {
        if (realm.isInTransaction())
            realm.commitTransaction();

        if (v.getId() == R.id.selectDep) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_asset_types);
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId() == R.id.addDep) {
            AssetTypeEditorFragment fragment = new AssetTypeEditorFragment();
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId() == R.id.btnAsset) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_assets, R.id.nav_asset_model, model.getId());
            fragment.setmListener(fragment);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }

    }


}
