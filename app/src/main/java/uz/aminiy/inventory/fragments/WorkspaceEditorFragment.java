package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import io.realm.Realm;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.adapters.AssetAdapter;
import uz.aminiy.inventory.databinding.FragmentWorkspaceEditorBinding;
import uz.aminiy.inventory.models.Department;
import uz.aminiy.inventory.models.Workspace;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link WorkspaceEditorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WorkspaceEditorFragment extends Fragment
        implements OnListFragmentInteractionListener,
        View.OnClickListener{

    private static final String WSP_ID = "param1";

    private String wspId;
    private Realm realm;
    private Workspace workspace;

    public void setmListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
        this.isListener = true;
    }

    private Boolean isListener=false;
    private OnListFragmentInteractionListener mListener;
    FragmentWorkspaceEditorBinding binding;

    public WorkspaceEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param wspid Parameter 1.
     * @return A new instance of fragment WorkspaceEditorFragment.
     */
    public static WorkspaceEditorFragment newInstance(String wspid) {
        WorkspaceEditorFragment fragment = new WorkspaceEditorFragment();
        Bundle args = new Bundle();
        args.putString(WSP_ID, wspid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();

        FragmentWorkspaceEditorBinding.inflate(getActivity().getLayoutInflater());

        Workspace wsp;

        if (getArguments() != null) {
            wsp = realm.where(Workspace.class).equalTo("id", getArguments().getString(WSP_ID)).findFirst();
        } else
            wsp = new Workspace();

        workspace = wsp;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_workspace_editor, container, false);
        binding.setWorkspace(workspace);

        View view = binding.getRoot();

        if (!realm.isInTransaction())
            realm.beginTransaction();

        if (isListener) {
            Button button;
            button = view.findViewById(R.id.imageButton);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(v -> {
                if (null != mListener) {
                    realm.copyToRealmOrUpdate(workspace);
                    realm.commitTransaction();
                    mListener.onListFragmentInteraction(workspace.getId());
                }
            });
        }

        ImageButton button = view.findViewById(R.id.selectDep);
        button.setOnClickListener(this);
        ImageButton button1 = view.findViewById(R.id.addDep);
        button1.setOnClickListener(this);
        Button button2 = view.findViewById(R.id.btnAsset);
        button2.setOnClickListener(this);

        if (!realm.isInTransaction())
            realm.beginTransaction();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener==null)
                mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        if (realm.isInTransaction()) {
            realm.copyToRealmOrUpdate(workspace);
            realm.commitTransaction();
        }

        super.onDestroyView();
    }

    @Override
    public void onListFragmentInteraction(String item) {
        if (item!=null) {
            realm.beginTransaction();
            Department dep = realm.where(Department.class).equalTo("id", item).findFirst();
            if (dep!=null) {
                workspace.setDepartment(dep);
                realm.copyToRealmOrUpdate(workspace);
                binding.setWorkspace(workspace);
            }
            realm.commitTransaction();
        }
        if (!realm.isInTransaction())
            realm.beginTransaction();

        getFragmentManager().popBackStack();

    }

    @Override
    public void onClick(View v) {
        if (realm.isInTransaction())
            realm.commitTransaction();

        if (v.getId()==R.id.selectDep) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_departments);
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId()==R.id.addDep) {
            DepartmentEditorFragment fragment = new DepartmentEditorFragment();
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId()==R.id.btnAsset) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_assets, R.id.nav_workspace, workspace.getId());
            fragment.setmListener(fragment);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }

    }

}
