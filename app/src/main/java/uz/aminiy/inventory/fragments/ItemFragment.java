package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.adapters.AssetAdapter;
import uz.aminiy.inventory.adapters.AssetCharacteristicAdapter;
import uz.aminiy.inventory.adapters.AssetModelAdapter;
import uz.aminiy.inventory.adapters.AssetTypeAdapter;
import uz.aminiy.inventory.adapters.CharacteristicFieldAdapter;
import uz.aminiy.inventory.adapters.DepartmentAdapter;
import uz.aminiy.inventory.adapters.SupplierAdapter;
import uz.aminiy.inventory.adapters.WorkspaceAdapter;
import uz.aminiy.inventory.models.Asset;
import uz.aminiy.inventory.models.AssetCharacteristic;
import uz.aminiy.inventory.models.AssetModel;
import uz.aminiy.inventory.models.AssetType;
import uz.aminiy.inventory.models.CharacteristicField;
import uz.aminiy.inventory.models.Department;
import uz.aminiy.inventory.models.Supplier;
import uz.aminiy.inventory.models.Workspace;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ItemFragment extends Fragment implements OnListFragmentInteractionListener, View.OnClickListener {

    private static final String NAV_NUMBER = "nav-number";
    private static final String SUB_NUMBER = "sub-number";
    private static final String OBJECT_ID = "asset-number";
    private int mNavNumber = 0;
    private int mSubNumber = 0;
    private String mObjectNumber = null;
    Realm realm;

    public void setmListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }

    private OnListFragmentInteractionListener mListener;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemFragment() {
    }

    public static ItemFragment newInstance(int nav_number) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(NAV_NUMBER, nav_number);
        fragment.setArguments(args);
        return fragment;
    }

    public static ItemFragment newInstance(String asset_number) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putString(OBJECT_ID, asset_number);
        fragment.setArguments(args);
        return fragment;
    }

    public static ItemFragment newInstance(int nav_number, String asset_number) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(NAV_NUMBER, nav_number);
        args.putString(OBJECT_ID, asset_number);
        fragment.setArguments(args);
        return fragment;
    }

    public static ItemFragment newInstance(int nav_number, int sub_number, String asset_number) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(NAV_NUMBER, nav_number);
        args.putInt(SUB_NUMBER, sub_number);
        args.putString(OBJECT_ID, asset_number);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mNavNumber = getArguments().getInt(NAV_NUMBER);
            mObjectNumber = getArguments().getString(OBJECT_ID);
            mSubNumber = getArguments().getInt(SUB_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        realm = Realm.getDefaultInstance();

        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);

        if (mSubNumber == R.id.nav_departments && mObjectNumber != null) {
            recyclerView.setAdapter(
                    new AssetAdapter(realm.where(Asset.class)
                            .equalTo("workspace.department.id", mObjectNumber)
                            .isNull("deleted_at").findAllAsync(),
                            true, mListener));
        } else if (mSubNumber == R.id.nav_workspace && mObjectNumber != null) {
            recyclerView.setAdapter(
                    new AssetAdapter(realm.where(Asset.class)
                            .equalTo("workspace.id", mObjectNumber)
                            .isNull("deleted_at").findAllAsync(),
                            true, mListener));
        } else if (mSubNumber == R.id.nav_suppliers && mObjectNumber != null) {
            recyclerView.setAdapter(
                    new AssetAdapter(realm.where(Asset.class)
                            .equalTo("supplier.id", mObjectNumber)
                            .isNull("deleted_at").findAllAsync(),
                            true, mListener));
        } else if (mSubNumber == R.id.nav_asset_model && mObjectNumber != null) {
            recyclerView.setAdapter(
                    new AssetAdapter(realm.where(Asset.class)
                            .equalTo("model.id", mObjectNumber)
                            .isNull("deleted_at").findAllAsync(),
                            true, mListener));
        }  else if (mSubNumber == R.id.nav_asset_types && mObjectNumber != null) {
            recyclerView.setAdapter(
                    new AssetAdapter(realm.where(Asset.class)
                            .equalTo("model.assettype.id", mObjectNumber)
                            .isNull("deleted_at").findAllAsync(),
                            true, mListener));
        } else if (mNavNumber == R.id.nav_departments) {
            recyclerView.setAdapter(new DepartmentAdapter(realm.where(Department.class)
                    .sort("name").isNull("deleted_at").findAllAsync(),
                    true, mListener));
        } else if (mNavNumber == R.id.nav_workspace) {
            recyclerView.setAdapter(new WorkspaceAdapter(realm.where(Workspace.class)
                    .sort("department.name").isNull("deleted_at").findAllAsync(),
                    true, mListener));
        } else if (mNavNumber == R.id.nav_asset_types) {
            recyclerView.setAdapter(new AssetTypeAdapter(realm.where(AssetType.class)
                    .isNull("deleted_at").findAllAsync(),
                    true, mListener));
        } else if (mNavNumber == R.id.nav_asset_model) {
            recyclerView.setAdapter(new AssetModelAdapter(realm.where(AssetModel.class)
                    .isNull("deleted_at").findAllAsync(),
                    true, mListener));
        } else if (mNavNumber == R.id.nav_all_characteristics) {
            recyclerView.setAdapter(
                    new CharacteristicFieldAdapter(realm.where(CharacteristicField.class)
                            .isNull("deleted_at").findAllAsync(),
                            true, mListener));
        } else if (mNavNumber == R.id.nav_suppliers) {
            recyclerView.setAdapter(
                    new SupplierAdapter(realm.where(Supplier.class)
                            .isNull("deleted_at").findAllAsync(),
                            true, mListener));
        } else if (mNavNumber == R.id.nav_assets) {
            recyclerView.setAdapter(
                    new AssetAdapter(realm.where(Asset.class)
                            .isNull("deleted_at").findAllAsync(),
                            true, mListener));
        } else if (mNavNumber == R.id.nav_asset_characteristics) {
            RealmQuery<AssetCharacteristic> query = realm.where(AssetCharacteristic.class).isNull("deleted_at");
            if (mObjectNumber != null) {
                recyclerView.setAdapter(
                        new AssetCharacteristicAdapter(query.equalTo("asset.id", mObjectNumber).findAll(),
                                true, mListener));
            }
        }

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener == null) {
                mListener = (OnListFragmentInteractionListener) context;
            }
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        if (mNavNumber == R.id.nav_departments) {
            DepartmentEditorFragment fragment = new DepartmentEditorFragment();
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_workspace) {
            WorkspaceEditorFragment fragment = new WorkspaceEditorFragment();
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_asset_types) {
            AssetTypeEditorFragment fragment = new AssetTypeEditorFragment();
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_asset_model) {
            AssetModelEditorFragment fragment = new AssetModelEditorFragment();
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_all_characteristics) {
            CharacteristicFieldEditorFragment fragment = new CharacteristicFieldEditorFragment();
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_suppliers) {
            SupplierEditorFragment fragment = new SupplierEditorFragment();
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_assets) {
            AssetEditorFragment fragment = new AssetEditorFragment();
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_asset_characteristics && mObjectNumber != null) {
            AssetCharacteristicEditorFragment fragment = AssetCharacteristicEditorFragment.newInstance(null, mObjectNumber);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onListFragmentInteraction(String item) {

        if (mNavNumber == R.id.nav_departments) {
            DepartmentEditorFragment fragment = DepartmentEditorFragment.newInstance(item);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_workspace) {
            WorkspaceEditorFragment fragment = WorkspaceEditorFragment.newInstance(item);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_asset_types) {
            AssetTypeEditorFragment fragment = AssetTypeEditorFragment.newInstance(item);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_asset_model) {
            AssetModelEditorFragment fragment = AssetModelEditorFragment.newInstance(item);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_all_characteristics) {
            CharacteristicFieldEditorFragment fragment = CharacteristicFieldEditorFragment.newInstance(item);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_suppliers) {
            SupplierEditorFragment fragment = SupplierEditorFragment.newInstance(item);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (mNavNumber == R.id.nav_assets) {
            AssetEditorFragment fragment = AssetEditorFragment.newInstance(item);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (this.mNavNumber == R.id.nav_asset_characteristics) {
            AssetCharacteristicEditorFragment fragment = AssetCharacteristicEditorFragment.newInstance(item);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }

    }
}
