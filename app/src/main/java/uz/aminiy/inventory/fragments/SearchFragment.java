package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import io.realm.Realm;
import io.realm.RealmQuery;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.adapters.AssetAdapter;
import uz.aminiy.inventory.barcode.AnyOrientationCaptureActivity;
import uz.aminiy.inventory.models.Asset;

public class SearchFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private Realm realm;
    private EditText search_text;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        search_text = view.findViewById(R.id.barcode_text);
        recyclerView = view.findViewById(R.id.recycler);

        Button button = view.findViewById(R.id.search_btn);
        button.setOnClickListener(this::OnFindClick);

        ImageButton scan_btn = view.findViewById(R.id.scan_barcode);
        scan_btn.setOnClickListener(this::OnScanClick);
        return view;
    }

    private void OnScanClick(View view){
        IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
        integrator.setCaptureActivity(AnyOrientationCaptureActivity.class);
        integrator.setOrientationLocked(false);
        integrator.initiateScan();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result!=null)
            search_text.setText(result.getContents());

        OnFindClick(search_text);
    }

    private void OnFindClick(View view){
        String text = search_text.getText().toString();
        RealmQuery<Asset> query = realm.where(Asset.class);
        query.equalTo("number", text);
        query.or().equalTo("serial_number", text);
        query.or().equalTo("name", text);
        query.or().equalTo("marker", text);

        recyclerView.setAdapter(
                new AssetAdapter(query.findAll(),true, mListener));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
