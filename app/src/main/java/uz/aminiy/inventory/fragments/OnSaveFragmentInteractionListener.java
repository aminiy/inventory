package uz.aminiy.inventory.fragments;

/**
 * Created by amin on 04.03.18.
 */

public interface OnSaveFragmentInteractionListener {
    void onSaveFragmentInteraction(String item);
}
