package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.lang.reflect.Type;
import java.util.Map;

import io.realm.Realm;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.barcode.AnyOrientationCaptureActivity;
import uz.aminiy.inventory.databinding.FragmentSyncBinding;
import uz.aminiy.inventory.v1api.AssetCharacteristicController;
import uz.aminiy.inventory.v1api.AssetController;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SyncFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SyncFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String BASE_URL;
    String TOKEN;

    FragmentSyncBinding binding;

    public void setmListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
    }

    private OnListFragmentInteractionListener mListener;

    public SyncFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SyncFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SyncFragment newInstance(String param1, String param2) {
        SyncFragment fragment = new SyncFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sync, container, false);

        Realm realm = Realm.getDefaultInstance();

        SharedPreferences pref = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        BASE_URL = pref.getString("server_url", "http://192.168.42.237/");
        TOKEN = pref.getString("token", "");

        binding.setUrl(BASE_URL);
        binding.setToken(TOKEN);

        View view = binding.getRoot();

        ImageButton button = view.findViewById(R.id.scan_qrcode_url);
        button.setOnClickListener(this);
        Button button1 = view.findViewById(R.id.save_url_settings);
        button1.setOnClickListener(this);
        Button button2 = view.findViewById(R.id.sync_data);
        button2.setOnClickListener(this);

        return binding.getRoot();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener == null) {
                mListener = (OnListFragmentInteractionListener) context;
            }
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.scan_qrcode_url) {
            IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
            integrator.setCaptureActivity(AnyOrientationCaptureActivity.class);
            integrator.setOrientationLocked(false);
            integrator.initiateScan();
        } else if (v.getId()==R.id.save_url_settings) {
            SharedPreferences pref = this.getActivity().getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            BASE_URL = binding.getUrl();
            TOKEN = binding.getToken();
            editor.putString("server_url", BASE_URL);
            editor.putString("token", TOKEN);
            editor.apply();;
        } else  if (v.getId()==R.id.sync_data) {
            AssetController controller = new AssetController(BASE_URL, TOKEN);
            controller.deleteData();
            controller.deleteRecords();
            controller.sendData();

            AssetCharacteristicController controller1 = new AssetCharacteristicController(BASE_URL, TOKEN);
            controller1.deleteData();
            controller1.deleteRecords();
            controller1.sendData();
            controller1.getData();
            controller.getData();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            Gson gson = new Gson();
            Type type = new TypeToken<Map<String, String>>(){}.getType();
            Map<String, String> map = gson.fromJson(result.getContents(), type);

            BASE_URL=map.get("url");
            TOKEN = map.get("token");
            binding.setUrl(BASE_URL);
            binding.setToken(TOKEN);
        }
    }

}

