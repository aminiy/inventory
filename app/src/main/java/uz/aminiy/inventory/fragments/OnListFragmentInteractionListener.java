package uz.aminiy.inventory.fragments;

/**
 * Created by amin on 04.03.18.
 */

public interface OnListFragmentInteractionListener {
    void onListFragmentInteraction(String item);
}
