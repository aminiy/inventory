package uz.aminiy.inventory.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.databinding.FragmentAssetCharacteristicEditorBinding;
import uz.aminiy.inventory.models.AssetCharacteristic;
import uz.aminiy.inventory.models.Asset;
import uz.aminiy.inventory.models.CharacteristicField;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link AssetCharacteristicEditorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AssetCharacteristicEditorFragment extends Fragment
        implements OnListFragmentInteractionListener, View.OnClickListener {

    private static final String ASC_ID = "asset_type_id";
    private static final String ASSETT_ID = "asset_id";

    private String getAstId;
    private int nav=0;

    private Realm realm;
    private AssetCharacteristic characteristic;
    private FragmentAssetCharacteristicEditorBinding binding;

    public void setmListener(OnListFragmentInteractionListener mListener) {
        this.mListener = mListener;
        this.isListener = true;
    }

    private Boolean isListener=false;
    private OnListFragmentInteractionListener mListener;

    public AssetCharacteristicEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param astid Parameter 1.
     * @return A new instance of fragment AssetEditorFragment.
     */
    public static AssetCharacteristicEditorFragment newInstance(String astid, String asset_id) {
        AssetCharacteristicEditorFragment fragment = new AssetCharacteristicEditorFragment();
        Bundle args = new Bundle();
        args.putString(ASC_ID, astid);
        args.putString(ASSETT_ID, asset_id);
        fragment.setArguments(args);
        return fragment;
    }

    public static AssetCharacteristicEditorFragment newInstance(String astid) {
        AssetCharacteristicEditorFragment fragment = new AssetCharacteristicEditorFragment();
        Bundle args = new Bundle();
        args.putString(ASC_ID, astid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();

        if (getArguments() != null) {
            String assetCharacteristicId=getArguments().getString(ASC_ID);
            String assetId=getArguments().getString(ASSETT_ID);

            if (assetCharacteristicId!=null) {
                characteristic = realm.where(AssetCharacteristic.class).equalTo("id", getArguments().getString(ASC_ID)).findFirst();
            }

            else {
                characteristic = new AssetCharacteristic();
                Asset asset = realm.where(Asset.class).equalTo("id", assetId).findFirst();
                if (asset!=null) {
                    characteristic.setAsset(asset);
                }
            }

        }
        else characteristic = new AssetCharacteristic();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_asset_characteristic_editor, container,false);
        binding.setCharacteristic(characteristic);

        View view = binding.getRoot();


        if (!realm.isInTransaction())
            realm.beginTransaction();

        if (isListener) {
            Button button;
            button = view.findViewById(R.id.imageButton);
            button.setVisibility(View.VISIBLE);
            button.setOnClickListener(v -> {
                if (null != mListener) {
                    realm.copyToRealmOrUpdate(characteristic);
                    realm.commitTransaction();
                    mListener.onListFragmentInteraction(characteristic.getId());
                }
            });
        }

        ImageButton button1 = view.findViewById(R.id.selectChar);
        button1.setOnClickListener(this);
        ImageButton button2 = view.findViewById(R.id.addChar);
        button2.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener==null)
                mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        if (realm.isInTransaction()) {
            realm.copyToRealmOrUpdate(characteristic);
            realm.commitTransaction();
        }

        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (realm.isInTransaction())
            realm.commitTransaction();

        if (v.getId()==R.id.selectChar) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_all_characteristics);
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId()==R.id.addChar) {
            CharacteristicFieldEditorFragment fragment = new CharacteristicFieldEditorFragment();
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        }

        nav=v.getId();
        if (!realm.isInTransaction())
            realm.beginTransaction();

    }

    @Override
    public void onListFragmentInteraction(String item) {
        if (item!=null) {
            realm.beginTransaction();

                CharacteristicField field = realm.where(CharacteristicField.class).equalTo("id", item).findFirst();
                if (field != null) {
                    characteristic.setCharacteristic(field);
                }


            realm.copyToRealmOrUpdate(characteristic);
            binding.setCharacteristic(characteristic);
            realm.commitTransaction();
        }
        if (!realm.isInTransaction())
            realm.beginTransaction();

        getFragmentManager().popBackStack();
    }
}
