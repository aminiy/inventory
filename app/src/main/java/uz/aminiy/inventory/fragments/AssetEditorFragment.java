package uz.aminiy.inventory.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import uz.aminiy.inventory.R;
import uz.aminiy.inventory.adapters.LinearAssetCharacteristics;
import uz.aminiy.inventory.barcode.AnyOrientationCaptureActivity;
import uz.aminiy.inventory.databinding.FragmentAssetEditorBinding;
import uz.aminiy.inventory.models.Asset;
import uz.aminiy.inventory.models.AssetModel;
import uz.aminiy.inventory.models.Supplier;
import uz.aminiy.inventory.models.Workspace;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link AssetEditorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AssetEditorFragment extends Fragment
        implements OnListFragmentInteractionListener,
        View.OnClickListener {

    private static final String AST_ID = "asset_type_id";

    private String getAstId;
    private int nav = 0;

    private Realm realm;
    private Asset asset;
    FragmentAssetEditorBinding binding;

    private OnListFragmentInteractionListener mListener;

    public AssetEditorFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param astid Parameter 1.
     * @return A new instance of fragment AssetEditorFragment.
     */
    public static AssetEditorFragment newInstance(String astid) {
        AssetEditorFragment fragment = new AssetEditorFragment();
        Bundle args = new Bundle();
        args.putString(AST_ID, astid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();

        if (getArguments() != null) {
            asset = realm.where(Asset.class).equalTo("id", getArguments().getString(AST_ID)).findFirst();
        } else asset = new Asset();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_asset_editor, container, false);
        realm = Realm.getDefaultInstance();
        binding.setAsset(asset);
        View view = binding.getRoot();

        RecyclerView recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setAdapter(new LinearAssetCharacteristics(asset.getCharacteristics(), true));


        if (!realm.isInTransaction())
            realm.beginTransaction();

        ImageButton setArrival = view.findViewById(R.id.setArrival);
        setArrival.setOnClickListener(this::selArrival);
        ImageButton button = view.findViewById(R.id.selectModel);
        button.setOnClickListener(this);
        ImageButton button1 = view.findViewById(R.id.addModel);
        button1.setOnClickListener(this);
        ImageButton button2 = view.findViewById(R.id.selectWorkspace);
        button2.setOnClickListener(this);
        ImageButton button3 = view.findViewById(R.id.addWorkspace);
        button3.setOnClickListener(this);
        ImageButton button4 = view.findViewById(R.id.selectSupplier);
        button4.setOnClickListener(this);
        ImageButton button5 = view.findViewById(R.id.addSupplier);
        button5.setOnClickListener(this);
        ImageButton button6 = view.findViewById(R.id.scanAssetNumber);
        button6.setOnClickListener(this);
        ImageButton button7 = view.findViewById(R.id.scanAssetSerial);
        button7.setOnClickListener(this);
        ImageButton button8 = view.findViewById(R.id.scanAssetMarker);
        button8.setOnClickListener(this);
        ImageButton button9 = view.findViewById(R.id.imageInventored);
        button9.setOnClickListener(this);
        ImageButton button10 = view.findViewById(R.id.allCharacteristics);
        button10.setOnClickListener(this);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            if (mListener == null)
                mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void selArrival(View view) {
        Calendar calendar = Calendar.getInstance();
        DatePickerDialog dialog = new DatePickerDialog(this.getContext(), (view1, year, month, dayOfMonth) -> {
            calendar.set(year, month, dayOfMonth);
            asset.setArrival_date(calendar.getTime());
            binding.setAsset(asset);
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dialog.show();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        if (realm.isInTransaction()) {
            if (asset != null)
                realm.copyToRealmOrUpdate(asset);
            realm.commitTransaction();
        }

        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (realm.isInTransaction())
            realm.commitTransaction();

        if (v.getId() == R.id.selectModel) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_asset_model);
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId() == R.id.addModel) {
            AssetModelEditorFragment fragment = new AssetModelEditorFragment();
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId() == R.id.selectSupplier) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_suppliers);
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId() == R.id.addSupplier) {
            SupplierEditorFragment fragment = new SupplierEditorFragment();
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId() == R.id.selectWorkspace) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_workspace);
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId() == R.id.addWorkspace) {
            WorkspaceEditorFragment fragment = new WorkspaceEditorFragment();
            fragment.setmListener(this);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId() == R.id.allCharacteristics) {
            ItemFragment fragment = ItemFragment.newInstance(R.id.nav_asset_characteristics, asset.getId());
            fragment.setmListener(fragment);
            getFragmentManager().beginTransaction().replace(R.id.your_placeholder, fragment).addToBackStack(null).commit();
        } else if (v.getId() == R.id.scanAssetNumber || v.getId() == R.id.scanAssetSerial || v.getId() == R.id.scanAssetMarker) {
            IntentIntegrator integrator = IntentIntegrator.forSupportFragment(this);
            integrator.setCaptureActivity(AnyOrientationCaptureActivity.class);
            integrator.setOrientationLocked(false);
            integrator.initiateScan();
        } else if (v.getId() == R.id.imageInventored) {
            if (!realm.isInTransaction()) {
                realm.beginTransaction();
                asset.setInventory_date(new Date());
                realm.copyToRealmOrUpdate(asset);
                binding.setAsset(asset);
            }

        }

        nav = v.getId();
        if (!realm.isInTransaction())
            realm.beginTransaction();

    }

    @Override
    public void onListFragmentInteraction(String item) {
        if (item != null) {
            realm.beginTransaction();
            if (nav == R.id.addModel || nav == R.id.selectModel) {
                AssetModel model = realm.where(AssetModel.class).equalTo("id", item).findFirst();
                if (model != null) {
                    asset.setModel(model);
                }
            } else if (nav == R.id.addSupplier || nav == R.id.selectSupplier) {
                Supplier supplier = realm.where(Supplier.class).equalTo("id", item).findFirst();
                if (supplier != null) {
                    asset.setSupplier(supplier);
                }
            } else if (nav == R.id.addWorkspace || nav == R.id.selectWorkspace) {
                Workspace workspace = realm.where(Workspace.class).equalTo("id", item).findFirst();
                if (workspace != null) {
                    asset.setWorkspace(workspace);
                }
            }

            realm.copyToRealmOrUpdate(asset);
            binding.setAsset(asset);
            realm.commitTransaction();
        }
        if (!realm.isInTransaction())
            realm.beginTransaction();

        getFragmentManager().popBackStack();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (!realm.isInTransaction())
                realm.beginTransaction();
            if (nav == R.id.scanAssetNumber) {
                asset.setNumber(result.getContents());
            }
            if (nav == R.id.scanAssetSerial) {
                asset.setSerial_number(result.getContents());
            }
            if (nav == R.id.scanAssetMarker) {
                asset.setMarker(result.getContents());
            }
            realm.copyToRealmOrUpdate(asset);
            binding.setAsset(asset);
            realm.commitTransaction();
        }

        if (!realm.isInTransaction())
            realm.beginTransaction();

    }
}
